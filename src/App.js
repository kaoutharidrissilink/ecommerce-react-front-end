import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import React from 'react'
import ProductList from './components/ProductList';
import Home from './components/Home';
import ProductDetail from './components/ProductDetail';
import 'bootstrap/dist/css/bootstrap.min.css';





class App extends React.Component {
  state = {
    products: []
  };

  async componentDidMount() {
    const response = await fetch('https://fakestoreapi.com/products');
    const body = await response.json();
    this.setState({products: body});
  }

  render() {
    return (
        <Router>
          <Switch>
          <Route path='/' exact={true} component={Home}/>
          <Route path='/products' exact={true} component={ProductList}/>
          <Route path='/products/:id' exact={true} component={ProductDetail}/>
        </Switch>
        </Router>
    )
}

}
export default App;
