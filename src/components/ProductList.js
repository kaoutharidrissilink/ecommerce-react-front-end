
import { Button, ButtonGroup, Container, Table } from 'reactstrap';
import AppNavbar from './AppNavbar';
import { Link } from 'react-router-dom';
import React from 'react'



class ProductList extends React.Component {

    constructor(props) {
        super(props);
        this.state = { products: [] };

    }

    componentDidMount() {
        fetch('https://fakestoreapi.com/products')
            .then(response => response.json())
            .then(data => this.setState({ products: data }));
    }
    render() {
        const { products, isLoading } = this.state;

        if (isLoading) {
            return <p>Loading...</p>;
        }

        const productList = products.map(product => {
            return <tr key={product.id}>
                <td style={{ whiteSpace: 'nowrap' }}>{product.title}</td>
                <td>{product.price}</td>
                <td>{product.description}</td>
                <td>{product.category}</td>
                <img src={product.image} alt="Product logo" />
                <td>

                    <ButtonGroup>

                        <Button size="sm" color="primary" tag={Link} to={"/products/" + product.id}>Product Details</Button>

                    </ButtonGroup>
                </td>
            </tr>
        });

        return (
            <div>
                <AppNavbar />
                <Container fluid>
                    <h3>Product Listing</h3>
                    <Table className="mt-4">
                        <thead>
                            <tr>
                                <th width="10%">Title</th>
                                <th width="10%">Price</th>
                                <th width="10%">Description</th>
                                <th width="10%">Category</th>
                                <th width="10%">Image</th>
                                <th width="20%">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {productList}
                        </tbody>
                    </Table>
                </Container>
            </div>
        );
    }
}
export default ProductList;