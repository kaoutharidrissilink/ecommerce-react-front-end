import React, { Component } from 'react';
import './App.css';
import AppNavbar from './AppNavbar';
import { Link } from 'react-router-dom';
import { Button, Container } from 'reactstrap';

class Home extends Component {

    state = { 
        isRedirect: false,
      }
    
      redirect = () => {
        this.setState({ isRedirect: true });
      }
    render() {
        return (
            <div>
                <AppNavbar/>
                <Container fluid>
                    <Button color="link"><Link to="/products">Products Listing</Link></Button>
                </Container>
            </div>
           
        );
        
    }
}
export default Home;