import React from 'react';
import { withRouter } from 'react-router-dom';




class ProductDetail extends React.Component {

    emptyItem = {
        title: '',
        price: ''
    };

    constructor(props) {
        super(props);
        this.state = {
            item: this.emptyItem
        };

    }

    async componentDidMount() {

        const product = await (await fetch(`https://fakestoreapi.com/products/${this.props.match.params.id}`)).json()
        this.setState({ item: product });

    }

    render() {
        const { item } = this.state;


        return (
            <div>
                <div class="container bootdey">
                    <div class="col-md-12">
                        <section class="panel">
                            <div class="panel-body">
                                <div class="col-md-6">
                                    <div class="pro-img-details">
                                        <img src={item.image} alt="Product logo" />
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <h4 class="pro-d-title">
                                        <a href="#/" class="">
                                            {item.title}
                                        </a>
                                    </h4>
                                    <p>
                                        {item.description}
                                    </p>
                                    <div class="product_meta">
                                        <span class="posted_in">
                                            <strong>Categories:</strong>
                                            <a rel="tag" href="#/">{item.category}</a>,
                                        </span>

                                    </div>
                                    <div class="m-bot15"> <strong>Price : </strong> <span class="amount-old">${item.price}</span>  <span class="pro-price"> ${item.price}</span></div>
                                    <div class="form-group">
                                        <label>Quantity</label>
                                        <input type="quantiy" placeholder="1" class="form-control quantity" />
                                    </div>
                                    <p>
                                        <button class="btn btn-round btn-danger" type="button"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
                                    </p>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        );
    }

}



export default withRouter(ProductDetail);